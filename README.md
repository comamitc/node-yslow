# node-yslow

## Docker Cloud Workflow

Build Image & Push 
```
$> docker build -t node-yslow .
$> docker login
$> docker tag node-yslow mcomardo/node-yslow:<version>
$> docker push mcomardo/node-yslow:<version>
```
